@extends('layouts.master')
@section('style')
<link href="{{asset('theme/backend/assets/plugins/jquery-dynatree/skin/ui.dynatree.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{asset('theme/backend/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" media="screen" />
<title>Upload Multiple Images using dropzone.js and Laravel</title>
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>--}}
@endsection
@section('content')

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">

            <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages -->
        @include('partial/error_message')
        <!-- END Error Messages -->
            <div class="tab-pane padding-20 active slide-left">
                <div class="row row-same-height">
                    <div class="panel-body">
                        <!-- START PANEL -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Product Photos
                                </div>
                                <div class="tools">
                                    <a class="collapse" href="javascript:;"></a>
                                    <a class="config" data-toggle="modal" href="#grid-config"></a>
                                    <a class="reload" href="javascript:;"></a>
                                    <a class="remove" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="panel-body no-scroll no-padding">
                                {!! Form::open([ 'url' => 'product/upload-photo', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
                                    <div class="fallback">
                                        <input name="file" type="file" multiple />
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <!-- END PANEL -->
                    </div>
                </div>
                {{ Form::open(['url' => 'product','method' => 'post', 'id' => 'form-project']) }}
                    @include('product.partial.form')
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection
@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-dynatree/jquery.dynatree.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/js/tree_category_script.js')}}" type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize         :       1,
            acceptedFiles: ".jpeg,.jpg,.png,.gif"
        };
    </script>
@endsection
