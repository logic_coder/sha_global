@extends('layouts.master')

@section('style')
    <link href="{{asset('theme/backend/assets/plugins/jquery-dynatree/skin/ui.dynatree.css')}}" rel="stylesheet" type="text/css" media="screen" />
    {{--<link href="{{asset('theme/backend/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" media="screen" />--}}
    <title>Upload Multiple Images using dropzone.js and Laravel</title>
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>--}}
@endsection
@section('content')
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <!-- START WIDGET -->
            <div class="row-xs-height">
                <div class="col-xs-height">
                    <div class="p-l-20 p-r-20">
                        <div class="row">
                            <div class="col-lg-3 visible-xlg">
                                <div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-xs-height">
                <div class="col-xs-height relative bg-master-lightest">
                    <div class="widget-14-chart_y_axis"></div>
                    <div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
                </div>
            </div>
            <!-- END WIDGET -->
        </div>

        <div id="rootwizard" class="m-t-50">

            <!-- Success Messages -->
        @include('partial/success_message')
        <!-- END Success Messages -->
            <!-- Error Messages -->
        @include('partial/error_message')
        <!-- END Error Messages -->
        <!-- START CONTAINER FLUID -->
                <div id="rootwizard" class="m-t-50">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
                        <li class="active">
                            <a data-toggle="tab" href="#tab1"><i class="fa tab-icon"></i> <span>Your Product</span></a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab2"><i class="fa fa-truck tab-icon"></i> <span>Shipping information</span></a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab3"><i class="fa fa-credit-card tab-icon"></i> <span>Payment details</span></a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#tab4"><i class="fa fa-check tab-icon"></i> <span>Summary</span></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane padding-20 active slide-left" id="tab1">
                            {{ Form::open(['url' => 'product','method' => 'post', 'id' => 'form-project', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone no-margin', 'id' => 'image-upload']) }}
                                {{ csrf_field() }}
                                @include('product.partial.form')
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane slide-left padding-20" id="tab2">
                        </div>
                        <div class="tab-pane slide-left padding-20" id="tab3">
                        </div>
                        <div class="padding-20 bg-white">
                            <ul class="pager wizard">
                                <li class="next">
                                    <button class="btn btn-success btn-cons pull-right" type="submit">
                                        <span>Save</span>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <!-- END CONTAINER FLUID -->
        </div>
    </div>
@endsection

@section('plugin-script')
    <script src="{{asset('theme/backend/assets/plugins/jquery-dynatree/jquery.dynatree.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('theme/backend/assets/js/tree_category_script.js')}}" type="text/javascript"></script>
    {{--<script src="{{asset('theme/backend/assets/plugins/dropzone/dropzone.min.js')}}" type="text/javascript"></script>--}}
@endsection



