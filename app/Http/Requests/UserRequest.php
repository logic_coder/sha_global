<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        $rules = [];  
        $rules = array_merge($rules,['email.required' => 'Mobile no or email is required']);
        $rules = array_merge($rules,['email.unique' => 'Mobile no or email already exist']);
        if( is_numeric($this->input('email') ) ){
            $rules = array_merge($rules,['email.regex' => 'Mobile no format is invalid']);
        }else if( !is_numeric($this->input('email') ) ){
            $rules = array_merge($rules,['email.regex' => 'Mobile no format is invalid']);
        }
        $rules = array_merge($rules,['email.email' => 'Email must be a valid email address']);
        $rules = array_merge($rules,['category_id.required' => 'Business type is required']);
        return $rules; 
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                $rules = [];      
                if( is_numeric($this->input('email') ) ){
                    $rules = array_merge($rules,['email' => 'required|regex:/(01)[0-9]{9}$/|unique:users,email']);
                }else if( !is_numeric($this->input('email') ) ){
                    $rules = array_merge($rules,['email' => 'required|email|unique:users,email']);
                }
                $rules = array_merge($rules,['password' => 'required|min:8|max:20']);
                $rules = array_merge($rules,['business_name' => 'sometimes|required|string|min:5|max:200|unique:companies,name']);
                $rules = array_merge($rules,['category_id' => 'sometimes|required']);
                return $rules;
                break;

            case 'PUT':
                $rules = [];
                if( is_numeric($this->input('email') ) ){
                    $rules = array_merge($rules,['email' => 'required|regex:/(01)[0-9]{9}$/|unique:users,email,'.$this->route('user')->id]);
                }else if( !is_numeric($this->input('email') ) ){
                    $rules = array_merge($rules,['email' => 'required|email|unique:users,email,'.$this->route('user')->id]);
                }
                return $rules;
                break;               
        }
    }
}
