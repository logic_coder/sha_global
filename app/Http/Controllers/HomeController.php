<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        /* Below code is for security reason in "Application Layer", If looged user has access on this module, only then user get this page. Otherwise it will redirect to page 401 */ 

        /*if(!Auth::user()->can('access-home-page')){
            abort(401);
        }*/        
        return view('home.index');
    }
}
