<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourPackagePrice extends Model
{
    use SoftDeletes;
    protected $table = 'tour_package_prices';
    protected $fillable = ['tour_package_id','minimum_paying_person' ,'standard','deluxe','luxury'];
    protected $dates = ['deleted_at'];
}
