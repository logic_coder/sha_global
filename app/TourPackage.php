<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourPackage extends Model
{
    use SoftDeletes;
    protected $table = 'tour_packages';
    protected $fillable = ['name','description'];
    protected $dates = ['deleted_at'];

    public function tourPackagePrice()
    {
        return $this->hasMany('App\TourPackagePrice');
    }
}
