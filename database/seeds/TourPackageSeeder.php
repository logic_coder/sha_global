<?php
use App\TourPackage;
use App\TourPackagePrice;
use Illuminate\Database\Seeder;

class TourPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tourPackage = array(
            'Package 1: 2D 1N' => "Day 1:Jaflong, Lalakhal, ShahparanMazar
								   Day 2:Bisanakhandi + Ratargul, ShahjalalMazar",
			'Package 2: 3D 2N Sylhet Tour' => "Day 1:Jaflong, Lalakhal, ShahparanMazar
												Day 2:Bisanakhandi + Ratargul, 
												ShahjalalMazar
												Day 3:ShadaPathor + Sylhet City Tour",
			'Package 3: 4D 3N Sylhet' => "Day 1:Jaflong, Lalakhal, ShahparanMazar
										  Day 2:Bisanakhandi + Ratargul, ShahjalalMazar
										  Day 3:ShadaPathor + Sylhet City Tour"
        );
        foreach( $tourPackage as $key => $value ){
            $tourPackageObj = new TourPackage();
            $tourPackageObj->name = $key;
            $tourPackageObj->description = $value;
            $tourPackageObj->save();
        }

        $tourPackagePrices = [ 
			['02-00 PAX','12840', '14640', '17160'],
			['03-04 PAX','9840', '11880', '15120'],
			['05-06 PAX','6840', '8640', '11760'],
			['07-08 PAX','5880', '7560', '10440'],
			['09-10 PAX','4680', '6600', '9360'],
        ];

        foreach( $tourPackagePrices as $key => $tourPackagePrice ){
			$tourPackagePriceObj = new TourPackagePrice();
            $tourPackagePriceObj->tour_package_id = "1";
            $tourPackagePriceObj->minimum_paying_person = $tourPackagePrice[0];
            $tourPackagePriceObj->standard = $tourPackagePrice[1];
            $tourPackagePriceObj->deluxe = $tourPackagePrice[2];
            $tourPackagePriceObj->luxury = $tourPackagePrice[3];
            $tourPackagePriceObj->save();
        }
    }
}
