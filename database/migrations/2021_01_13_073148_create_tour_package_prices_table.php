<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateTourPackagePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('tour_package_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_package_id')->unsigned();
            $table->text('minimum_paying_person');
            $table->decimal('standard', 10, 2);
            $table->decimal('deluxe', 10, 2);
            $table->decimal('luxury', 10, 2);            
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tour_package_id')->references('id')->on('tour_packages')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_prices');
    }
}
